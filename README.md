<h2>Author: Guilherme de Souza Topanotti</h2>

<h3 dir="auto">Janeiro de 2020</h3>

Descrição:
<p dir="auto"><span style="font-size:14px">Exemplo simples de autenticação  utilizando spring JWT&nbsp;e H2DB.</span></p>


-----------------
Basta utilizar usuário: guilherme senha: 123, fazer a requisição para /login conforme doc.

O token irá retornar no header da request, após isto, basta inseri-lo no header Authorization para 
 os demais endpoins
 ----------------------

<p dir="auto">&nbsp;</p>

<h2 dir="auto">Documenta&ccedil;&atilde;o da api:</h2>

<p dir="auto"><span style="font-size:14px">http://localhost:8080/api/swagger-ui.html</span></p>

<h2 dir="auto">Para executar:</h2>
mvn clean install 

mvn spring-boot:run
