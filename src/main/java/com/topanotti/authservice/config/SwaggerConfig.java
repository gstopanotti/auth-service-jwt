package com.topanotti.authservice.config;

import io.swagger.models.auth.In;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(metadada())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.topanotti"))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .securitySchemes(Arrays.asList(new ApiKey("Authorization", HttpHeaders.AUTHORIZATION, In.HEADER.name())));

    }

    private ApiInfo metadada() {
        return new ApiInfoBuilder()
                .title("Autenticação Spring Boot com JWT")
                .description("\"Exemplo de autenticação utilizando Spring Boot, REST API e JWT \"")
                .version("1.0.0")
                .contact(new Contact("Guilherme Topanotti", null, "gstopanotti@gmail.com"))
                .build();
    }
}

