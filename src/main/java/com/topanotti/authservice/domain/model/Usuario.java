package com.topanotti.authservice.domain.model;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
public class Usuario implements UserDetails {

    @ApiModelProperty(value = "Login do usuário")
    @Id
    @NotNull
    private String login;

    @ApiModelProperty(value = "Nome do usuário")
    @NotNull
    private String nome;

    @ApiModelProperty(value = "Senha do usuário")
    @NotNull
    private String senha;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @ApiModelProperty(hidden= true)
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @ApiModelProperty(hidden= true)
    @Override
    public String getPassword() {
        return this.senha;
    }

    @ApiModelProperty(hidden= true)
    @Override
    public String getUsername() {
        return this.login;
    }

    @ApiModelProperty(hidden= true)
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @ApiModelProperty(hidden= true)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @ApiModelProperty(hidden= true)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @ApiModelProperty(hidden= true)
    public boolean isEnabled() {
        return true;
    }

}
