package com.topanotti.authservice.api;

import com.topanotti.authservice.dto.LoginUsuarioDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/login")
@Api(tags = "Login de usuários")
public class LoginController {


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um token no header para o usuário autenticar "),
            @ApiResponse(code = 403, message = "Usuário ou senha incorreta"),
            @ApiResponse(code = 500, message = "Erro interno do servidor")
    })
    @ResponseHeader(name = "Authorization", description = "Token de autenticação do usuário")
    @PostMapping(produces = "application/json")
    public void autenticar(@RequestBody LoginUsuarioDto loginUsuario) {

    }


}
