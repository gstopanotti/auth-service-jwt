package com.topanotti.authservice.service;

import com.topanotti.authservice.domain.model.Usuario;
import com.topanotti.authservice.domain.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;


@Repository
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.buscar(login)
                .orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado"));

        return usuario;
    }
}
