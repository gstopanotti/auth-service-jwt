insert into usuario values ('guilherme','guilherme topanotti', '$2a$10$DEnLuzIWI5CoFJ8pQ4dHwemxGFrC4eWduWbMyKYFpClNxl17nSTQO');
CREATE INDEX IF NOT EXISTS usuario_index ON usuario(login);